from tkinter import *
from tkinter import ttk 
import tkinter as tk
import random

# Classe gérant la fenêtre du jeu et les action impactant le plateau de jeu.
class Puyopuyo :
    #Variable globales pour les dimmensions du plateau de jeu
    HEIGHT = 240
    WIDTH = 120
    #Fontion pour initialiser le jeu
    def start(self,colone,ligne,tik,level):
        self.new_game = True
        self.colone = colone
        self.ligne = ligne
        self.tik = tik
        self.level = level
        self.pause = False
        self.score = 0
        Puyopuyo.HEIGHT = self.ligne*Puyo.PUYO_SIZE
        Puyopuyo.WIDTH = self.colone*Puyo.PUYO_SIZE

        self.root = Tk()
        self.root.geometry("450x550")
        self.root.title("Puyo puyo")
        self.grille = [[0]*self.ligne for i in range(self.colone)]
        self.score_l = Label(self.root,text = self.score )
        self.canvas = Canvas(self.root, bg ="black", height = self.ligne*Puyo.PUYO_SIZE, width = self.colone*Puyo.PUYO_SIZE)
        self.pnext = Canvas(self.root, bg ="black", height = 40, width = 40*2)
        self.pnext2 = Canvas(self.root, bg ="black", height = 40, width = 40*2)
        self.bgauche = ttk.Button(self.root,width = 5, text = "⬅️")
        self.bbas = ttk.Button(self.root,width = 5, text = "⬇️")
        self.bdroit = ttk.Button(self.root,width = 5, text = "➡️")
        self.bpause = ttk.Button(self.root,width = 0,text = "⏸",command = lambda : self.pausef())

        self.bgauche.place(x = 10, y = 520)
        self.bbas.place(x = 100, y = 520)
        self.bdroit.place(x = 190, y = 520)
        self.canvas.place(x = 10 , y = 10)
        self.bpause.place(x = self.colone*Puyo.PUYO_SIZE + 140, y = 10)

        #Check de la difficulter pour savoir combien de canvas afficher
        #pour les prochaines pièces
        if self.level != "difficile":
            self.pnext.place(x = self.colone*Puyo.PUYO_SIZE + 20,y = 10)
            if self.level != "normal":
                self.pnext2.place(x = self.colone*Puyo.PUYO_SIZE + 60,y = 60)
        self.score_l.place(x = 230 , y = 490)
        
        #Binding des touches du clavier et appel de la fonction de jeu principale
        self.root.bind("<Key>", self.handle_events)
        self.tick()
        self.root.mainloop()
    
    #Fonction recursive principale gérant le jeu
    def tick(self):
        #Initialisation si la partie vien de ce lancer
        if self.new_game == True:
            self.current_p = Puyo(self.canvas,self.grille)
            self.next_p = Puyo(self.pnext)
            self.next_p2 = Puyo(self.pnext2)
            self.bbas.config(command = lambda : self.mv(self.current_p,0,1))
            self.bgauche.config(command = lambda : self.mv(self.current_p,-1,0))
            self.bdroit.config(command = lambda : self.mv(self.current_p,1,0))
            self.new_game = False

        #Si le mouvement vers le bas n'est plus possible et que la partie n'est pas perdu
        #essaye d'effacer des puyos si possible et de les faire chuter pars la suite
        #et selectionne un nouveau couples de puyos. Si la partie est perdu met le jeu en pause et affiche le menu de fin
        if not self.current_p.mouvement_possible_puyos(0,1):
            if not self.lost(self.current_p):
                while self.erase(self.current_p.grille) :
                    self.drop(self.current_p.grille)
                    self.redraw(self.current_p)
                    self.score_l.config(text = self.score)
                self.current_p = Puyo(self.canvas,self.grille,self.next_p.pp[0],self.next_p.pp[1])
                self.next_p = Puyo(self.pnext,None,self.next_p2.pp[0],self.next_p2.pp[1])
                self.next_p2 = Puyo(self.pnext2)
            else:
                if self.pause == False:
                    self.ecran_fin()
        elif self.pause == False :
            self.current_p.chute()
        #Après un laps de temps tick rappel la même fonction
        self.root.after(self.tik, self.tick)

    #Fonction pour mettre le jeu en pause
    def pausef(self):
        if self.pause == False:
            self.pause = True
            self.bpause.config(text = "▶️")
        else :
            self.pause = False
            self.bpause.config(text = "⏸")
    
    def mv(self,puyo,x,y):
        puyo.mv(x,y)
    def handle_events(self, event):
        if event.keysym == "Left": self.mv(self.current_p,-1,0)
        if event.keysym == "Right": self.mv(self.current_p,1,0)
        if event.keysym == "Down": self.mv(self.current_p,0,1)

    #Si les deux première cases du tableau son occuper (les puyos apparaisent en haut à gauche) 
    #retourne vrais
    def lost(self,puyo):
        if puyo.grille[0][0] != 0 or puyo.grille[1][0] != 0:
            return True
        return False

    #Fonction recursive retournant un tableau 
    #avec les coordonnées des puyos etant de la même couleur et voisin du puyo en paramètre
    def same_puyo(self,cur, same,puyo):
        dxy = ((1, 0), (0, 1), (-1, 0), (0, -1))
        same.append(cur)
        for di in dxy:
            nex = (cur[0] + di[0], cur[1] + di[1])
            if 0 <= nex[0] < self.colone and 0 <= nex[1] < self.ligne \
                    and nex not in same \
                    and puyo[nex[0]][nex[1]] == puyo[cur[0]][cur[1]]:
                self.same_puyo(nex, same,puyo)
        return same
    
    #Redessine le plateau de jeu
    def redraw(self,puyo):
        puyo.canvas.delete("all")
        for y in range(self.ligne):
            for x in range(self.colone):
                if puyo.grille[x][y] != 0:
                    puyo.canvas.create_rectangle(x*Puyo.PUYO_SIZE,y*Puyo.PUYO_SIZE,(x+1)*Puyo.PUYO_SIZE,(y+1)*Puyo.PUYO_SIZE,fill =Puyo.color(puyo.grille[x][y]))

    #Parcours le plateau de jeu en appelant la fonction same_puyo()
    #si la longueur du tableau retourner pars same_puyo() et plus grand ou égal a 4
    #efface les puyos dont les coordonnées sont stocker dans le tableau.
    def erase(self,puyo):
        more_drop = False
        for y in range(self.ligne):
            for x in range(self.colone):
                if puyo[x][y] == 0:
                    continue
                same = self.same_puyo((x, y), [],puyo)
                if len(same) < 4:
                    continue
                for p in same:
                    more_drop = True
                    puyo[p[0]][p[1]] = 0
                    self.score += 100
        return more_drop

    #Parcours du tableau à l'envers dans l'axe Y
    #descend les puyos qui on un case vide en dessous d'eux.
    def drop(self,puyo):
        for y in range(self.ligne)[::-1]:
            for x in range(0, self.colone):
                if puyo[x][y] == 0:
                    for y2 in range(0, y)[::-1]:
                        if puyo[x][y2] != 0:
                            puyo[x][y] = puyo[x][y2]
                            puyo[x][y2] = 0
                            break

    #Creation d'une fenêtre mettant le jeu en pause
    #et proposant au joueur de rejouer ou de revenir au menu principal
    def ecran_fin(self):
        self.pause = True
        s = "Score : "
        s += str(self.score)
        window_f = Toplevel(self.root)
        window_f.title("Perdu !")
        window_f.geometry("300x300")

        replay = ttk.Button(window_f,text = "Rejouer", width = 15, command = lambda : replay())
        main = ttk.Button(window_f,text = "Menu Principal", width = 15, command = lambda : menu())

        score_l = Label(window_f,text = s)

        score_l.place(x = 120 , y = 50)
        replay.place(x = 75 , y = 80)
        main.place(x = 75 , y = 130)

        def replay():
            game = Puyopuyo()
            window_f.destroy()
            self.root.destroy()
            game.start(self.colone,self.ligne,self.tik)
        def menu():
            menu = Menu()
            window_f.destroy()
            self.root.destroy()
            menu.start()
            

#Classe créant et affichant les puyos
#contient les fontions de mouvements pour les puyos
class Puyo : 
    #Variable global pour la taille en pixel des puyos.
    PUYO_SIZE = 40
    #Fonction d'initialisation qui place un couple de puyos sur un canvas selon les paramètres.
    #Les paramètre grille,p1,p2 sont vides (None) si ils ne sont pas spécifier lors de l'appel de la fonction.
    #Cela permet de la surcharge et faire en une fonction ce que 3 fonctions ferrais.
    def __init__(self,canvas,grille = None,p1 = None, p2 = None):
        self.puyos = []
        #Les paramètre p1 et p2 reprèsente les couleurs de deux puyos
        #Cela permet de passer un couples d'un canvas à l'autre. 
        if p1 is None :
            self.pp = self.next_puyo()
        else :
            self.pp = [p1, p2]
        self.canvas = canvas
        self.grille = grille
        self.i = 0
        for p in self.pp :
            puyo = canvas.create_rectangle(
                self.i*Puyo.PUYO_SIZE,
                0*Puyo.PUYO_SIZE,
                (self.i+1)*Puyo.PUYO_SIZE,
                1*Puyo.PUYO_SIZE,
                fill = Puyo.color(p))
            if grille is not None :
                self.grille[self.i][0] = p
            self.i += 1
            self.puyos.append(puyo)

    #Retourne deux puyos de façon aléatoire
    def next_puyo(self) :
        couleur = ["V","B","J","R"]
        return [random.choice(couleur) for i in range(2)]

    #Equivalent de switch case pour python
    #retourne une couleur selon le paramètre x
    #revois la couleur noire pars défaut.
    def color(x):
        return {
            'V': "green",
            'B': "blue",
            'R': "red",
            'J': "yellow"
        }.get(x, "black")

    #Déplace les puyos d'une case vers le bas.
    #utilise la fonction coords() de tkinter pour avoir les coordonnées en pixels et les divise pars la taille en pixels
    #afin d'avoir la position dans le tableau en mémoire.
    def chute(self):
        if not self.mouvement_possible_puyos(0,1):
            return False
        else:
            for puyo in self.puyos:
                coords = self.canvas.coords(puyo)
                self.canvas.move(puyo, 0 * Puyo.PUYO_SIZE,1 * Puyo.PUYO_SIZE)
                self.grille[int(coords[0] / Puyo.PUYO_SIZE)][int(coords[1] / Puyo.PUYO_SIZE) + 1] = self.grille[int(coords[0] / Puyo.PUYO_SIZE)][int(coords[1] / Puyo.PUYO_SIZE)]
                self.grille[int(coords[0] / Puyo.PUYO_SIZE)][int(coords[1] / Puyo.PUYO_SIZE)] = 0

    #fonction de déplacements en x,y 
    #si le déplacement est vers la droite déplace le deuxième puyo en premier
    #afin que le premier n'écrase pas le deuxième.
    def mv(self , x , y):
        if not self.mouvement_possible_puyos(x,y):
            return False
        else:
            if x > 0 :
                for puyo in reversed(self.puyos):
                    coords = self.canvas.coords(puyo)
                    self.canvas.move(puyo, x * Puyo.PUYO_SIZE,y * Puyo.PUYO_SIZE)
                    self.grille[int(coords[0] / Puyo.PUYO_SIZE) + x][int(coords[1] / Puyo.PUYO_SIZE) + y] = self.grille[int(coords[0] / Puyo.PUYO_SIZE)][int(coords[1] / Puyo.PUYO_SIZE)]
                    self.grille[int(coords[0] / Puyo.PUYO_SIZE)][int(coords[1] / Puyo.PUYO_SIZE)] = 0
            else :
                for puyo in self.puyos:
                    coords = self.canvas.coords(puyo)
                    self.canvas.move(puyo, x * Puyo.PUYO_SIZE,y * Puyo.PUYO_SIZE)
                    self.grille[int(coords[0] / Puyo.PUYO_SIZE) + x][int(coords[1] / Puyo.PUYO_SIZE) + y] = self.grille[int(coords[0] / Puyo.PUYO_SIZE)][int(coords[1] / Puyo.PUYO_SIZE)]
                    self.grille[int(coords[0] / Puyo.PUYO_SIZE)][int(coords[1] / Puyo.PUYO_SIZE)] = 0
    
    #Vérification si le mouvement est possible pour un puyo seul.
    def mouvement_possible_puyo(self, puyo, x, y):
        x = x * Puyo.PUYO_SIZE
        y = y * Puyo.PUYO_SIZE
        coords = self.canvas.coords(puyo)

        if coords[3] + y > Puyopuyo.HEIGHT: 
            return False
        if coords[0] + x < 0 : return False
        if coords[2] + x > Puyopuyo.WIDTH: 
            return False

        overlap = set(self.canvas.find_overlapping(
                (coords[0] + coords[2]) / 2 + x, 
                (coords[1] + coords[3]) / 2 + y, 
                (coords[0] + coords[2]) / 2 + x,
                (coords[1] + coords[3]) / 2 + y
                ))
        other_items = set(self.canvas.find_all()) - set(self.puyos)
        if overlap & other_items: return False

        return True
    
    #Vérification du movement pour un couple de puyo.
    def mouvement_possible_puyos(self,x,y):
        if not self.puyos: 
            return False
        elif y == 0 :
           for puyo in self.puyos:
                if not self.mouvement_possible_puyo(puyo, x, y): return False 
        else :
            i = 0
            for puyo in self.puyos:
                if not self.mouvement_possible_puyo(puyo, x, y):
                    self.puyos.pop(i)
                    break
                i += 1
        return True

#Classe gérant le menu principale et la fenêtre de paramètre.
class Menu :
    def start(self) :
        self.root = Tk()
        self.root.title("Puyo puyo")
        self.root.geometry("500x500")
        self.ligne = 12
        self.colone = 6 
        self.tick = 400
        self.level = "normal"

        self.titre = Label(self.root, text = "Puyo Puyo")
        self.play_btn = ttk.Button(self.root, text="Jouer", command=self.new_game)
        self.param_btn = ttk.Button(self.root, text="Paramètres", command=self.param_w)
        self.exit_btn = ttk.Button(self.root, text="Quitter", command=lambda:self.root.destroy())

        self.titre.place(x=215, y=150)
        self.param_btn.place(x=100, y=250, width=300)
        self.play_btn.place(x=100, y=200, width=300)
        self.exit_btn.place(x=100, y=300, width=300)

        self.root.mainloop()

    #Fonction d'affichage et de sélection des paramètres
    def param_w(self):
        def update(event):
            self.level = n.get()

        n = tk.StringVar()
        window_p = Toplevel(self.root)
        window_p.title("Paramètres")
        window_p.geometry("300x300")

        ligne_l = Label(window_p,text="Lignes")
        colone_l = Label(window_p,text="Colonnes")
        tick_l = Label(window_p,text="Vitesse") 
        choix_l = Label(window_p,text="Difficulté")
        ligne = Entry(window_p,width=20)
        colone = Entry(window_p,width=20)
        tick = Entry(window_p,width=35)
        save = ttk.Button(window_p,text="Sauvegarder les changements",width=35,command = lambda : self.save(int(colone.get()),int(ligne.get()),int(tick.get()),window_p))
        choix = ttk.Combobox(window_p,width = 35, textvariable = n)
        
        choix['values'] =('facile','normal','difficile')
        choix.current(1)
        choix.bind("<<ComboboxSelected>>", update)



        ligne.insert(0,self.ligne)
        colone.insert(0,self.colone)
        tick.insert(0,self.tick)

        ligne_l.place(x=50,y=0)
        colone_l.place(x=200,y=0)
        ligne.place(x=0,y=20)
        colone.place(x=150,y=20)
        tick_l.place(x=110,y=45)
        tick.place(x=0,y=65)
        choix.place(x=0,y=115)
        choix_l.place(x=110,y=95)
        save.place(x=0,y=280)

    #Validation des paramètres
    def save(self,colone,ligne,tick,wp):
        self.colone = colone
        self.ligne = ligne
        self.tick = tick
        wp.destroy()

    #Lancement du jeu avec les paramètres selectionnés dans le menu
    def new_game(self):
        game = Puyopuyo()
        self.root.destroy()
        game.start(self.colone,self.ligne,self.tick,self.level)

#Lancement du menu principal au lancement du programme
if __name__ == "__main__":
    game = Menu()
    game.start()
